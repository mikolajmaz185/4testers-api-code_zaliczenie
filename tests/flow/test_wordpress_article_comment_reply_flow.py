import time
from base64 import b64encode
import requests
import pytest
import lorem


# Editor
username = 'editor'
password = 'HWZg hZIP jEfK XCDE V9WM PQ3t'
blog_url = 'https://gaworski.net'
posts_endpoint_url = blog_url + "/wp-json/wp/v2/posts"
token = b64encode(f"{username}:{password}".encode('utf-8')).decode("ascii")

# Commenter
commenter_username = 'commenter'
commenter_password = 'SXlx hpon SR7k issV W2in zdTb'
comments_endpoint_url = blog_url + "/wp-json/wp/v2/comments"
commenter_token = b64encode(f"{commenter_username}:{commenter_password}".encode('utf-8')).decode("ascii")


@pytest.fixture(scope='module')
def article():
    timestamp = int(time.time())
    article = {
        "article_creation_date": timestamp,
        "article_title": "This is new post " + str(timestamp),
        "article_subtitle": lorem.sentence(),
        "article_text": lorem.paragraph()
    }
    return article


@pytest.fixture(scope='module')
def headers():
    headers = {
        "Content-Type": "application/json",
        "Authorization": "Basic " + token
    }
    return headers


@pytest.fixture(scope='module')
def posted_article(article, headers):
    payload = {
        "title": article["article_title"],
        "excerpt": article["article_subtitle"],
        "content": article["article_text"],
        "status": "publish"
    }
    response = requests.post(url=posts_endpoint_url, headers=headers, json=payload)
    if response.status_code != 201:
        print("Error posting article:", response.status_code, response.text)
    return response


@pytest.fixture(scope='module')
def comment(article, posted_article):
    return {
        "post": posted_article.json()['id'],
        "author_name": commenter_username,
        "author_email": "commenter@example.com",
        "author_url": "",
        "content": lorem.sentence()
    }


@pytest.fixture(scope='module')
def commenter_headers():
    headers = {
        "Content-Type": "application/json",
        "Authorization": "Basic " + commenter_token
    }
    return headers


@pytest.fixture(scope='module')
def posted_comment(comment, commenter_headers):
    response = requests.post(url=comments_endpoint_url, headers=commenter_headers, json=comment)
    if response.status_code != 201:
        print("Error posting comment:", response.status_code, response.text)
        print("Comment payload:", comment)
    return response


@pytest.fixture(scope='module')
def reply(posted_comment):
    return {
        "post": posted_comment.json()['post'],
        "parent": posted_comment.json()['id'],
        "author_name": username,
        "author_email": "editor@example.com",
        "content": lorem.sentence()
    }


@pytest.fixture(scope='module')
def posted_reply(reply, headers):
    response = requests.post(url=comments_endpoint_url, headers=headers, json=reply)
    if response.status_code != 201:
        print("Error posting reply:", response.status_code, response.text)
        print("Reply payload:", reply)
    return response


def test_new_post_is_successfully_created(posted_article):
    assert posted_article.status_code == 201
    assert posted_article.reason == "Created"
    assert posted_article.json()['author'] == 2


def test_comment_is_successfully_created(posted_comment):
    assert posted_comment.status_code == 201
    assert posted_comment.reason == "Created"
    assert posted_comment.json()['author_name'] == commenter_username
    assert posted_comment.json()['post'] == posted_comment.json()['post']


def test_reply_is_successfully_created(posted_reply):
    assert posted_reply.status_code == 201
    assert posted_reply.reason == "Created"
    assert posted_reply.json()['author_name'] == username
    assert posted_reply.json()['parent'] == posted_reply.json()['parent']
    assert posted_reply.json()['post'] == posted_reply.json()['post']
